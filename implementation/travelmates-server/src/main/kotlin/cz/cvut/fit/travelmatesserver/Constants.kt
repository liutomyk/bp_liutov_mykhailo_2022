package cz.cvut.fit.travelmatesserver

/**
 * Storage for constant values
 */
object Constants {
    //Url for verifying user's auth token
    const val JWKS_URL = "https://cognito-idp.us-east-2.amazonaws.com/us-east-2_SSgxGyUmI"
}